#!/usr/bin/env python3

import argparse

import requests
import lxml.html

def scrape_num():
    '''Scrape the number of published records from the SIGEC website.'''
    
    home_url = 'http://www.catalogo.beniculturali.it/sigecSSU_FE/'

    r = requests.get(home_url)
    root = lxml.html.fromstring(r.text)
    meta = root.xpath('head/meta[@http-equiv="Refresh"]')[0]

    url_frag = meta.attrib['content'][6:]
    epoch_ms = url_frag.split('=')[1] # including milliseconds
    full_url = home_url + url_frag

    r2 = requests.get(full_url)
    root2 = lxml.html.fromstring(r2.text)
    testoh = root2.xpath('//div[@id="testoH"]/span')[0].text
    numero = testoh.split()[0]
    return epoch_ms, numero

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('-o', '--output', type=argparse.FileType('a'))
    args = vars(parser.parse_args())
    now, num = scrape_num()
    with args['output'] as f:
        f.write('{},{}\n'.format(now, num))

if __name__ == '__main__':
            main()
